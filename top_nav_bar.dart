import 'dart:math';

import 'package:flutter/material.dart';

class Nav extends StatelessWidget {
  @override
  Widget build(BuildContext context) => DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            title: Text('Home Screen'),
            bottom: TabBar(
              tabs: [
                Tab(text: 'Home'),
                Tab(text: 'Favour', icon: Icon(Icons.star)),
                Tab(text: 'Profile', icon: Icon(Icons.person)),
              ],
            ),
          ),
          body: TabBarView(
            children: [
              Center(child: Text('Home content here')),
              Center(child: Text('Favour content here')),
              Center(child: Text('Profile content here')),
            ],
          ),
        ),
      );
}
